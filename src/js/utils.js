
let jQuery = require('jquery')


export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Async get method
export const get = async (url) => {

    let result = undefined

    await jQuery.ajax(url).then( 
        (data, status, xhr) => {
            // console.log(data, status, xhr)   
            console.log(xhr)   
            result = data                     
        }
        , (error) => { // error handler
            // console.log("AJAX failed") 
            throw error
        }
    )


    return result
}

// Get for paginated data, loops through all pages and buffers data until last page when it returns
export const getAll = async (url, onPageReceived = null) => {

    let dataBuffer = []
    let next = url

    while (next ){

        await get(next)
            .then( (data) => {
                // NOTE: push mutates dataBuffer, but we are only updating the state with it once it is full
                // console.log (data)
                dataBuffer.push(...data.results)

                if (onPageReceived){
                    onPageReceived(next, dataBuffer)
                }

                next = data.next

            })
            .catch( (error) => { 

                next = undefined
                throw error
            })
        
        await sleep(3000)
            
        
    }

    return dataBuffer
}

/*
// Use as promise
get()
    .then( (result) => console.log(result) )
    .catch( (error) => console.log(error) )
*/