import {MyClass} from "./myclass"

let anInstance = new MyClass()


import React, { Component } from "react";  
import ReactDom from 'react-dom';

import * as Utils from "./utils"

/*
 * A simple React component
 */
class Application extends Component {

  
  constructor(props, context) {
    // Must call parent constructor
    super(props, context);

    // Initialise component state.  Modifying this will trigger a render.
    //   Only use this.setState to modify the state after this.    
    this.state = {
		  displayData: '[Empty]'
          , displayPeople: []
          , lastAction: ""
    }

  }

  // Called once when this component first becomes active
  componentDidMount() {

   
    
    // NOTE: After constructor, only modify the state using: 
    //          this.setState({...this.state, a: 1, b: 2, c: 3})
    //
    //      This will ensure that state changes do not mutate the state 
    //      but rather replace it with a modified copy which will trigger a re-render
    //
    //      For appending to arrays: this.setState({...this.state, myArray: [...this.state.myArray, newValue]})

    // Changes to this.state will automatically trigger a re-render
    // State changes should not mutate the state, they should replace it    
    Utils.get("http://swapi.co/api/people/1")
        .then( (data) => {
            this.setState({
                ...this.state, // Initialises new state with copy of the current state
                displayData: "Result: " + data.name // Add/Update displayData
            })
        })
        .catch( (error) => { 
            this.setState({
                ...this.state, 
                displayData: "Error: " + JSON.stringify(error)
            })
        })

    // Re-renders current data each time a new page arrives
    const onReceive = (url, currentBufferReference) => {
                    
        // Create a display list from results
        let updatedPeople = currentBufferReference.map(
            (person, i) => ( // map takes a callback with item and index as parameters

                <a class="list-group-item" key={i} href={person.url}> 
                    <h4 class="list-group-item-heading">{person.name}</h4>

                    <p class="list-group-item-text">More text...</p>
                    
                </a>
            ) // must give every item in a display list a 'key' value.  React will log errors if you don't. 
        )

        // Add display list to the state
        this.setState({
            ...this.state, 
            displayPeople: [...updatedPeople]
            , lastAction: url
        })
    }

    
    Utils.getAll("http://swapi.co/api/people", onReceive)
        .then((people) => {
            console.log("All pages retrieved successfully: ", people)
        })
        .catch( (error) => { 
            this.setState({
                ...this.state, 
                displayPeople: "Error: " + JSON.stringify(error)
            })
        })

        
  }

  // Called once when this component is removed
  componentWillUnmount(){

  }

  // Called whenever the state changes
  render() {

    // The following 2 lines are equivalent
    // let displayData = this.state.displayData
    let {displayData, displayPeople, lastAction} = this.state
      
    return <div>
      <h1>Hello ES6, async/await and React!</h1>      
      <p>
        <br/>
            NOTE: Refresh to trigger get.
        <br/>
            Last action: {lastAction}
        <br/>
        <br/>
        <br/>
            {displayData}
        <br/>
      </p>
      <div>     

        <div class="container-fluid full-height text-center">
            <div class="row full-height">
            
                <div class="col-md-12 full-height">  
                    <div class="well"  >
                        <div><p>Star Wars People</p></div>
                        <div class="list-group" style={{maxHeight: "300px", overflowY: "scroll"}}>
                        {displayPeople}
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
      </div>
    </div>;
  }
}

// Render the above component into the div#app
ReactDom.render(<Application />, document.getElementById('app1'));
