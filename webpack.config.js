var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path'); 

module.exports = {
  context: path.join(__dirname, "src"),
  devtool: debug ? "inline-sourcemap" : null,
  entry: "./js/scripts.js",
  module: {
    loaders: [
      {
        test: /\.(jsx?|js)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
          plugins: ['react-html-attrs',
                    // http://babeljs.io/docs/plugins/transform-runtime/
                    // support async / await amongst other things
                    ['transform-runtime', {
                        "polyfill": false,
                        "regenerator": true}]
          ]
        }
      }
    ]
  },
  output: {
    path: __dirname + "/webpack-static",
    filename: "scripts.min.js"
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin()
    , new webpack.optimize.OccurenceOrderPlugin()
    , new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false })
  ],
};
