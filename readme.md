# ES6, Async/Await and React Basic Example
## Environment
---
Tested on Mint 18 (Ubuntu 16.04) 64-bit

## Dependencies
---
### System-wide installations

    sudo apt install npm nodejs-legacy virtualenv

    npm install -g yarn # If you don't have yarn native package installed globally from their website (which is recommended)

## Launchers
---

### Install JS Dependencies
    yarn

### Launch webpack in watch mode
CTRL+c to stop webpack

    yarn start 

or

    yarn webpack-watch

For a production build, set production env variable, see webpack.config.js to understand what effect this has.
Production builds just uglify, minify and dedupe the code.

    NODE_ENV=production yarn start

### Run webpack once
    yarn webpack

### To view
    firefox index.html

### To clean
    yarn clear # NOT yarn clean
